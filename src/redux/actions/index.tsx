import * as ApplicationActions from './ApplicationActions';
import * as BerthActions from './BerthActions';
import * as FormActions from './FormActions';

export default {
  BerthActions,
  FormActions,
  ApplicationActions,
};
