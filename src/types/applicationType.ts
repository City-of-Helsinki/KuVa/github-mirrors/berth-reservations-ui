export enum ApplicationOptions {
  NewApplication = 'new_application',
  ExchangeApplication = 'exchange_application',
}

export enum ApplicationType {
  BerthApp = 'berths',
  WinterStorageApp = 'winter-storage',
  UnmarkedWinterStorageApp = 'unmarked-winter-storage',
}
