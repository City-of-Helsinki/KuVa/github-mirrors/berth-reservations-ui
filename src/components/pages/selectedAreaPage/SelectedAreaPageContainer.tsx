import get from 'lodash/get';
import React from 'react';
import { connect } from 'react-redux';
import { compose } from 'recompose';

import {
  deselectWinterArea,
  moveWinterAreaDown,
  moveWinterAreaUp,
} from '../../../redux/actions/WinterAreaActions';
import { WinterAreasQuery } from '../../../utils/__generated__/WinterAreasQuery';
import {
  getResources,
  getSelectedResources,
  getWinterStorageFilterByValues,
} from '../../../utils/berths';
import { LocalePush, withMatchParamsHandlers } from '../../../utils/container';
import SelectedAreaPage from './SelectedAreaPage';

import { WINTER_AREAS_QUERY } from '../../../utils/graphql';

import { Store } from '../../../redux/types';
import { SelectedWinterServices } from '../../../types/services';
import { WinterFormValues } from '../../../types/winterStorage';
import { SelectedIds } from '../../berths/types';
import { StepType } from '../../../common/steps/step/Step';
import { Query } from 'react-apollo';

interface Props {
  selectedAreas: SelectedIds;
  selectedServices: SelectedWinterServices;
  localePush: LocalePush;
  values: WinterFormValues;
  deselectArea(id: string): void;
  moveUp(id: string): void;
  moveDown(id: string): void;
}

const steps: StepType[] = [
  {
    completed: true,
    current: false,
    label: 'site.steps.winter_areas',
    linkTo: `winter-storage`,
  },
  {
    completed: false,
    current: true,
    label: 'site.steps.review_areas',
    linkTo: '',
  },
  {
    completed: false,
    current: false,
    label: 'site.steps.boat_information',
    linkTo: '',
  },
  {
    completed: false,
    current: false,
    label: 'site.steps.applicant',
    linkTo: '',
  },
  {
    completed: false,
    current: false,
    label: 'site.steps.send_application',
    linkTo: '',
  },
];

const UnconnectedSelectedAreaPage = ({
  localePush,
  values,
  selectedAreas,
  selectedServices,
  ...rest
}: Props) => {
  const moveToForm = async () => {
    await localePush('/winter-storage/form/registered-boat');
  };
  const handlePrevious = async () => {
    await localePush('/winter-storage');
  };
  return (
    <Query<WinterAreasQuery> query={WINTER_AREAS_QUERY}>
      {({
        // error, TODO: handle errors
        data,
      }) => {
        const width = get(values, 'boatWidth', '');
        const length = get(values, 'boatLength', '');
        const filter = getWinterStorageFilterByValues(values, selectedServices);
        const areas = getResources(data ? data.winterStorageAreas : null);
        const selected = getSelectedResources(selectedAreas, areas);
        const validSelection = selected.every(filter);

        return (
          <SelectedAreaPage
            boatInfo={{ width, length }}
            handlePrevious={handlePrevious}
            moveToForm={moveToForm}
            filter={filter}
            validSelection={validSelection}
            steps={steps}
            legend={{
              title: 'legend.selected_areas.title',
              legend: 'legend.selected_areas.legend',
            }}
            selectedAreas={selected}
            values={values}
            {...rest}
          />
        );
      }}
    </Query>
  );
};

export default compose<Props, Props>(
  withMatchParamsHandlers,
  connect(
    (state: Store) => ({
      selectedAreas: state.winterAreas.selectedWinterAreas,
      selectedServices: state.winterAreas.selectedWinterServices,
      values: state.forms.winterValues,
    }),
    {
      deselectArea: deselectWinterArea,
      moveUp: moveWinterAreaUp,
      moveDown: moveWinterAreaDown,
    }
  )
)(UnconnectedSelectedAreaPage);
