import React from 'react';
import './formLegend.scss';
import { useTranslation } from 'react-i18next';

type StepLegend = {
  title: string;
  legend: string;
};

interface Props {
  legend: StepLegend;
}

export default ({ legend }: Props) => {
  const { t } = useTranslation();
  return (
    <div className="vene-form-legend">
      <h3>{t(legend.title)}</h3>
      <p>{t(legend.legend)}</p>
    </div>
  );
};
